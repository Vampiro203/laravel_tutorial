<?php

namespace tutorial;

use Illuminate\Database\Eloquent\Model;

class Categoria extends Model
{
	//declarar tablas
    protected $table = 'categoria';

    //declarar primarykeys
    protected $primaryKey ="idcatedoria";

    //creacion y modificacion del registro

    public $timestamps = false;

    protected $fillable = ['nombre', 'descripcion', 'condicion'];

    protected $guarded = [];
}
