<?php

namespace tutorial\Http\Controllers;

use app\

use tutorial\Http\Requests;

use tutorial\Categoria;

use Illuminate\Support\Facades\Redirect;

use tutorial\Http\Requests\CategoriaForaRequest;

class CategoriaController extends Controller
{
    public function __construct(){

    }

    public function index(Requests $request){
    	if($request){
    		//trim quitar espacios al inicio y final
    		$query=trim($request->get('searchText'));
    		$categorias=DB::table('categoria')->where('nombre',LIKE,'%'.$query.'%')->where('condicion','=',1)->orderBy('idcategoria',desc)->paginate(7);
    		return view('almacen.categoria.index',["categorias"=>$categorias,"searchText"=>$query]);

    	}
    }

    public function create(){
    	return view("almacen.categoria.create");
    }

    public function store(CategoriaforaRequest $request){
    	$categoria=new Categoria;
    	$categoria->nombre=$request->get('nombre');
    	$categoria->descripcion=$request->get('descripcion');
    	$categoria->condicion='1';
    	$categoria->save();
    	return Redirect::to('almacen/categoria');
    }

    public function show($id){
    	return view("almacen.categoria.show",["categoria"=>Categoria::findOrFail($id)]);
    }

    public function edit($id){
    	return view("almacen.categoria.edit",["categoria"=>Categoria::findOrFail($id)]);
    }

    public function update(CategoriaforaRequest $request,$id){
    	$categoria=Categoria::findOrFail($id);
    	$categoria->nombre=$request->get('nombre');
    	$categoria->descripcion=$request->get('descripcion');
    		$categoria->update();
    		return Redirect::to('almacen/categoria');

    }

    public function destroy($id){
    	$categoria=Categoria::findOrFail($id);
    	$categoria->condicion='0';
    	$categoria->update();
    	return Redirect::to('almacen/categoria');

    }


}
