<?php

use App\vampires;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('about', function () {
    return view('about');
});


Route::get('name', function () {
    return view('name', [
    		'name' => 'Lestat'
    	]);
});

/*
Route::get('name', function () {
    return view('name')->with('name','Lestat');
});
*/

/*
Route::get('about', function () {

	$name='Lestat';
    return view('name', compact('name'));
});
*/

/*Route::get('/vampires', function () {

	

//$vampires=DB::table('vampires')->get();
$vampires=vampires::all();



   return view('vampires.index', compact('vampires'));
});*/

/*Route::get('/vampires/{vamp}', function ($id) {

	

//$vamp=DB::table('vampires')->find($id);
$vamp = vampires::find($id);



   return view('vampires.show', compact('vamp'));
});*/


route::get('/vampires', 'vampiresController@index');

route::get('/vampires/{vamp}', 'vampiresController@show');