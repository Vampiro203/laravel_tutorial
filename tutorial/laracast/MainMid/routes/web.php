<?php


$stripe= (resolve('App\Billing\Stripe'));




Route::get('/', 'campeonesController@Index')->name('home');


Route::get('/posts/create', 'campeonesController@create');

Route::post('/posts', 'campeonesController@store');

Route::get('/posts/{post}', 'campeonesController@show');

Route::post('/posts/{post}/comments', 'CommentsController@store');

Route::get('/register', 'RegistrationController@create');

Route::post('/register', 'RegistrationController@store');

Route::get('/login', 'SessionController@create');

Route::post('/login', 'SessionController@store');

Route::get('/logout', 'SessionController@destroy');
