<?php

namespace App\Http\Controllers;

use App\User;

use App\Mail\Welcome;

use Mail;

use App\Http\Requests\RegistrationRequest;

class RegistrationController extends Controller
{
    public function create(){
    	return view ('registration.create');
    }

    public function store(RegistrationRequest $request){

    	//validate the form

    	//create the user

    	$user = User::create([ 
            'name' => request('name'),
            'email' => request('email'),
            'password' => bcrypt(request('password'))
        ]);

    	//sig them in
    	auth()->login($user);

        \Mail::to($user)->send(new Welcome($user));


        session()->flash('message', 'Ya estas inscrito');


    	//redirect
    	return redirect()->home();
    }
}

