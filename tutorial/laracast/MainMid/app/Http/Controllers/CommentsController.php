<?php

namespace App\Http\Controllers;

use App\campeones;

use App\Comment;

class CommentsController extends Controller
{
	public function store(campeones $post){

		$this->validate(request(),['comentario'=>'required|min:2']);
		$post->addComment(request('comentario'));

		return back();
	}
    
}
