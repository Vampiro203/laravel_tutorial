<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\campeones;

use Carbon\Carbon;

class CampeonesController extends Controller
{

    public function __construct(){
        $this->middleware('auth')->except(['index', 'show']);
    }

    public function index(){



    		$posts=campeones::latest()->filter(request(['month','year']))->get();

           



    	return view('posts.index', compact ('posts'));

    }

    public function show(campeones $post){

    	return view('posts.show', compact('post'));

    }

      public function create(){
    	return view('posts.create');

    }

    public function store(){
    	//create a new post using the request data
    	//save it to the database
    	//and then redirect

    	//dd(request()->all());

    	//$post = new campeones;

    	//$post->nombre = request('nombre');

    	//$post->descripcion = request('descripcion');

    	//$post->save();

    	$this->validate(request(),[
    		'nombre'=>'required',
    		'descripcion'=>'required'
    	]);

        auth()->user()->publish(new campeones(request(['nombre', 'descripcion'])));

        session()->flash('message', 'Tu post se ha publicado');    

    	return redirect('/');



    }
}
