@component('mail::message')
# Introduction

Hola Noob

@component('mail::button', ['url' => 'https://lan.op.gg/l=es'])
Go!
@endcomponent

@component('mail::panel', ['url' => ''])
Red Hot Chili Peppers
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
